from flask import Flask, jsonify, json, Response, request
from flask_cors import CORS
import json

# A very basic API created using Flask that has two possible routes for requests.

app = Flask(__name__)
CORS(app)

@app.route("/")
def healthCheckResponse():
    return jsonify({"message" : "Nothing here, used for health check. Try /uncard instead."})

@app.route("/uncard")
def getUncard():

    response = Response(json.dumps({"message": "Flavia's cool API call"}))

    # set the Content-Type header so that the browser is aware that the response
    # is formatted as JSON and our frontend JavaScript code is able to
    # appropriately parse the response.
    response.headers["Content-Type"]= "application/json"

    return response


# Run the service on the local server it has been deployed to,
# listening on port 8080.
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)