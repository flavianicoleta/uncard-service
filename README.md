Replace REPLACE_ME_ACCOUNT_ID with your account ID and REPLACE_ME_REGION with your default region in the following 
command to build the docker image using the file Dockerfile,
which contains Docker instructions. 
The command tags the Docker image, using the -t option, with a specific tag format
so that the image can later be pushed to the Amazon Elastic Container Registry service.

Once you have your Account ID, you are ready to build the docker image:

`docker build . -t 313059860395.dkr.ecr.eu-west-1.amazonaws.com/uncard/service:latest`

Copy the image tag that resulted from the previous command and run the following command to deploy the container “locally”

`docker run -p 8080:8080 {image-tag}`

`$(aws ecr get-login-password --region eu-west-1) `

```
aws ecr get-login-password \
              --region eu-west-1 \
          | docker login \
              --username AWS \
              --password-stdin 313059860395.dkr.ecr.eu-west-1.amazonaws.com
```

Push image

```docker push 313059860395.dkr.ecr.eu-west-1.amazonaws.com/uncard/service:latest```

https://yy67388nlk.execute-api.eu-west-1.amazonaws.com/uncard